﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Top1000StackTags.Models;

namespace Top1000StackTags.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> StackTags()
        {
            try
            {
                //get tags
                var stackTags = await GetStackTags();
                //set count summary
                ViewData["CountSummary"] = stackTags.Item2;
                return View(stackTags.Item1);
            }
            catch (Exception ex)
            {
                //redirect to error page
                return RedirectToAction("Error", new { errorMsg = ex.Message });
            }

        }

        public async Task<Tuple<List<Tag>, int>> GetStackTags()
        {
            //create new empty tag list
            List<Tag> tags = new List<Tag>();
            int countSummary = 0;
            int tagNo = 1;
            //get 1000 tags
            for (int i = 1; i <= 10; i++)
            {
                //define api url
                string apiUrl = $"https://api.stackexchange.com/2.2/tags?page={i}&pagesize=100&order=desc&sort=popular&site=stackoverflow";
                //create request
                using (HttpClient client = new HttpClient())
                using (HttpResponseMessage res = await client.GetAsync(apiUrl))
                //get response content
                using (HttpContent content = res.Content)
                {
                    //get data
                    string data = await content.ReadAsStringAsync();
                    //parse to object
                    JObject dataTags = JObject.Parse(data);
                    //get tags from data
                    IList<JToken> results = dataTags["items"].ToList();
                    if (results != null)
                    {
                        for (int j = 0; j < results.Count; j++)
                        {
                            Tag tag = new Tag
                            {
                                //set tag number
                                TagId = tagNo,
                                //set tag name
                                Name = results[j]["name"].ToString(),
                                //set tag count
                                Count = Convert.ToInt32(results[j]["count"].ToString()),
                            };
                            //add tag count to summary
                            countSummary += tag.Count;
                            //add tag to the list
                            tags.Add(tag);
                            //increase tag id
                            tagNo++;
                        }
                    }
                }
            }
            return new Tuple<List<Tag>, int>(tags, countSummary);
        }

        public IActionResult Error(string errorMsg)
        {
            return View();
        }
    }
}
