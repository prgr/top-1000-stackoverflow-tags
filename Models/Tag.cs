﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Top1000StackTags.Models
{
    public class Tag
    {
        public int TagId { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
        public double PercentUsed { get; set; }
    }
}
